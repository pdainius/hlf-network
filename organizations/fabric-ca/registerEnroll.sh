#!/bin/bash

function createOrgKTU() {
  infoln "Enrolling the CA admin"
  mkdir -p organizations/peerOrganizations/ktu.dainius.dev/

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/ktu.dainius.dev/

  set -x
  fabric-ca-client enroll \
    -u https://admin:adminpw@localhost:7054 \
    --caname ca.ktu.dainius.dev \
    --tls.certfiles "${PWD}/organizations/fabric-ca/ktu/tls-cert.pem"
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/ca-ktu-dainius-dev.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/ca-ktu-dainius-dev.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/ca-ktu-dainius-dev.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/ca-ktu-dainius-dev.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/msp/config.yaml"

  infoln "Registering peer0.ktu.dainius.dev"
  set -x
  fabric-ca-client register \
    --caname ca.ktu.dainius.dev \
    --id.name peer0.ktu.dainius.dev \
    --id.secret peer0pwsecurepassword \
    --id.type peer \
    --tls.certfiles "${PWD}/organizations/fabric-ca/ktu/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering the org admin"
  set -x
  fabric-ca-client register \
    --caname ca.ktu.dainius.dev \
    --id.name ktuadmin \
    --id.secret ktuadminpw \
    --id.type admin \
    --tls.certfiles "${PWD}/organizations/fabric-ca/ktu/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Generating the peer0.ktu.dainius.dev msp"
  set -x
  fabric-ca-client enroll \
    -u https://peer0.ktu.dainius.dev:peer0pwsecurepassword@localhost:7054 \
    -M "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/peers/peer0.ktu.dainius.dev/msp" \
    --caname ca.ktu.dainius.dev \
    --csr.hosts peer0.ktu.dainius.dev \
    --tls.certfiles "${PWD}/organizations/fabric-ca/ktu/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/msp/config.yaml" \
     "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/peers/peer0.ktu.dainius.dev/msp/config.yaml"

  infoln "Generating the peer0-tls certificates"
  set -x
  fabric-ca-client enroll \
    -u https://peer0.ktu.dainius.dev:peer0pwsecurepassword@localhost:7054 \
    -M "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/peers/peer0.ktu.dainius.dev/tls" \
    --caname ca.ktu.dainius.dev \
    --enrollment.profile tls \
    --csr.hosts peer0.ktu.dainius.dev \
    --csr.hosts ca.ktu.dainius.dev \
    --tls.certfiles "${PWD}/organizations/fabric-ca/ktu/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/peers/peer0.ktu.dainius.dev/tls/tlscacerts/"* \
     "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/peers/peer0.ktu.dainius.dev/tls/ca.crt"

  cp "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/peers/peer0.ktu.dainius.dev/tls/signcerts/"* \
     "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/peers/peer0.ktu.dainius.dev/tls/server.crt"

  cp "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/peers/peer0.ktu.dainius.dev/tls/keystore/"* \
     "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/peers/peer0.ktu.dainius.dev/tls/server.key"

  mkdir -p "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/msp/tlscacerts"
  cp "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/peers/peer0.ktu.dainius.dev/tls/tlscacerts/"* \
     "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/msp/tlscacerts/ca.crt"

  mkdir -p "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/tlsca"

  cp "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/peers/peer0.ktu.dainius.dev/tls/tlscacerts/"* \
     "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/tlsca/tlsca.ktu.dainius.dev-cert.pem"

  mkdir -p "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/ca"
  cp "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/peers/peer0.ktu.dainius.dev/msp/cacerts/"* \
     "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/ca/ca.ktu.dainius.dev-cert.pem"

  infoln "Generating the org admin msp"
  set -x
  fabric-ca-client enroll \
    -u https://ktuadmin:ktuadminpw@localhost:7054 \
    -M "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/users/Admin@ktu.dainius.dev/msp" \
    --caname ca.ktu.dainius.dev \
    --tls.certfiles "${PWD}/organizations/fabric-ca/ktu/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/msp/config.yaml" \
     "${PWD}/organizations/peerOrganizations/ktu.dainius.dev/users/Admin@ktu.dainius.dev/msp/config.yaml"
}

function createOrderer() {
  infoln "Enrolling the CA admin"
  mkdir -p organizations/ordererOrganizations/dainius.dev

  export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/ordererOrganizations/dainius.dev

  set -x
  fabric-ca-client enroll \
    -u https://admin:adminpw@localhost:9054 \
    --caname ca.orderer.dainius.dev \
    --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/ca-orderer-dainius-dev.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/ca-orderer-dainius-dev.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/ca-orderer-dainius-dev.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/ca-orderer-dainius-dev.pem
    OrganizationalUnitIdentifier: orderer' > "${PWD}/organizations/ordererOrganizations/dainius.dev/msp/config.yaml"

  infoln "Registering orderer"
  set -x
  fabric-ca-client register \
    --caname ca.orderer.dainius.dev \
    --id.name orderer \
    --id.secret ordererpw \
    --id.type orderer \
    --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Registering the orderer admin"
  set -x
  fabric-ca-client register \
    --caname ca.orderer.dainius.dev \
    --id.name ordererAdmin \
    --id.secret ordererAdminpw \
    --id.type admin \
    --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  infoln "Generating the orderer msp"
  set -x
  fabric-ca-client enroll \
    -u https://orderer:ordererpw@localhost:9054 \
    --caname ca.orderer.dainius.dev \
    -M "${PWD}/organizations/ordererOrganizations/dainius.dev/orderers/orderer.dainius.dev/msp" \
    --csr.hosts orderer.dainius.dev \
    --csr.hosts ca.orderer.dainius.dev \
    --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/ordererOrganizations/dainius.dev/msp/config.yaml" \
     "${PWD}/organizations/ordererOrganizations/dainius.dev/orderers/orderer.dainius.dev/msp/config.yaml"

  infoln "Generating the orderer-tls certificates"
  set -x
  fabric-ca-client enroll \
    -u https://orderer:ordererpw@localhost:9054 \
    --caname ca.orderer.dainius.dev \
    -M "${PWD}/organizations/ordererOrganizations/dainius.dev/orderers/orderer.dainius.dev/tls" \
    --enrollment.profile tls \
    --csr.hosts orderer.dainius.dev \
    --csr.hosts ca.orderer.dainius.dev \
    --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/ordererOrganizations/dainius.dev/orderers/orderer.dainius.dev/tls/tlscacerts/"* \
     "${PWD}/organizations/ordererOrganizations/dainius.dev/orderers/orderer.dainius.dev/tls/ca.crt"

  cp "${PWD}/organizations/ordererOrganizations/dainius.dev/orderers/orderer.dainius.dev/tls/signcerts/"* \
     "${PWD}/organizations/ordererOrganizations/dainius.dev/orderers/orderer.dainius.dev/tls/server.crt"

  cp "${PWD}/organizations/ordererOrganizations/dainius.dev/orderers/orderer.dainius.dev/tls/keystore/"* \
     "${PWD}/organizations/ordererOrganizations/dainius.dev/orderers/orderer.dainius.dev/tls/server.key"

  mkdir -p "${PWD}/organizations/ordererOrganizations/dainius.dev/orderers/orderer.dainius.dev/msp/tlscacerts"

  cp "${PWD}/organizations/ordererOrganizations/dainius.dev/orderers/orderer.dainius.dev/tls/tlscacerts/"* \
     "${PWD}/organizations/ordererOrganizations/dainius.dev/orderers/orderer.dainius.dev/msp/tlscacerts/tlsca.dainius.dev-cert.pem"

  mkdir -p "${PWD}/organizations/ordererOrganizations/dainius.dev/msp/tlscacerts"

  cp "${PWD}/organizations/ordererOrganizations/dainius.dev/orderers/orderer.dainius.dev/tls/tlscacerts/"* \
     "${PWD}/organizations/ordererOrganizations/dainius.dev/msp/tlscacerts/tlsca.dainius.dev-cert.pem"

  infoln "Generating the admin msp"
  set -x
  fabric-ca-client enroll \
    -u https://ordererAdmin:ordererAdminpw@localhost:9054 \
    --caname ca.orderer.dainius.dev \
    -M "${PWD}/organizations/ordererOrganizations/dainius.dev/users/Admin@dainius.dev/msp" \
    --tls.certfiles "${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem"
  { set +x; } 2>/dev/null

  cp "${PWD}/organizations/ordererOrganizations/dainius.dev/msp/config.yaml" \
     "${PWD}/organizations/ordererOrganizations/dainius.dev/users/Admin@dainius.dev/msp/config.yaml"
}