#!/bin/bash

function one_line_pem {
    echo "`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $1`"
}

function json_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        -e "s/\${MSPID}/$6/" \
        -e "s/\${DOMAIN}/$7/" \
        -e "s/\${PEER_NAME}/$8/" \
        -e "s/\${CA_NAME}/$9/" \
        organizations/ccp-template.json
}

function yaml_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        -e "s/\${MSPID}/$6/" \
        -e "s/\${DOMAIN}/$7/" \
        -e "s/\${PEER_NAME}/$8/" \
        -e "s/\${CA_NAME}/$9/" \
        organizations/ccp-template.yaml | sed -e $'s/\\\\n/\\\n          /g'
}

ORG=$1
DOMAIN=$2
MSPID=$3
P0PORT=$4
CAPORT=$5
PEERPEM=$6
CAPEM=$7
PEER_NAME=$8
CA_NAME=$9

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $MSPID $DOMAIN $PEER_NAME $CA_NAME)" > organizations/peerOrganizations/ktu.dainius.dev/connection-ktu.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $MSPID $DOMAIN $PEER_NAME $CA_NAME)" > organizations/peerOrganizations/ktu.dainius.dev/connection-ktu.yaml