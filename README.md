## Running Bakalauras Network

To start the networks use the command `./network.sh up`, to stop a network, write `./netdown.sh down`. For more commands, enter `./network.sh` and all of the modes and flags will be presented.